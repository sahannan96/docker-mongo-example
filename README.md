Steps:

1. git clone https://gitlab.com/sahannan96/docker-mongo-example
2. cd docker-mongo-example
3. npm i
4. docker-compose -p mongo-example -f docker/docker-compose.yml up -d
5. npm run start:debug
6. visit _localhost:3000/api_ for swagger endpoints
7. _mongo -u root --authenticationDatabase admin -p admin_ to view mongodb
