FROM node:alpine
COPY . /app
WORKDIR /app
RUN npm install && npm run build && npm install --production
CMD ["node", "dist/main"]